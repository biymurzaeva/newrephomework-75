import {
	FETCH_ENCODED_MESSAGE_FAILURE,
	FETCH_ENCODED_MESSAGE_REQUEST,
	FETCH_ENCODED_MESSAGE_SUCCESS
} from "../actions/encodedMessageAction";

const initialState = {
	loading: false,
	message: {}
};

const encodedMessageReducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_ENCODED_MESSAGE_REQUEST:
			return {...state, loading: true};
		case FETCH_ENCODED_MESSAGE_SUCCESS:
			return {...state, loading: false, message: action.payload};
		case FETCH_ENCODED_MESSAGE_FAILURE:
			return {...state, loading: false}
		default:
			return state
	}
};

export default encodedMessageReducer;