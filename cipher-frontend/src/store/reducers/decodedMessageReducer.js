import {
	FETCH_DECODED_MESSAGE_FAILURE,
	FETCH_DECODED_MESSAGE_REQUEST,
	FETCH_DECODED_MESSAGE_SUCCESS
} from "../actions/decodedMessageAction";

const initialState = {
	loading: false,
	message: {}
};

const decodedMessageReducer = (state = initialState, action) => {
	switch (action.type) {
		case FETCH_DECODED_MESSAGE_REQUEST:
			return {...state, loading: true};
		case FETCH_DECODED_MESSAGE_SUCCESS:
			return {...state, loading: false, message: action.payload};
		case FETCH_DECODED_MESSAGE_FAILURE:
			return {...state, loading: false}
		default:
			return state
	}
};

export default decodedMessageReducer;