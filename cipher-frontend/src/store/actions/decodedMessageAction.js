import axios from "axios";

export const FETCH_DECODED_MESSAGE_REQUEST = 'FETCH_DECODED_MESSAGE_REQUEST';
export const FETCH_DECODED_MESSAGE_SUCCESS = 'FETCH_DECODED_MESSAGE_SUCCESS';
export const FETCH_DECODED_MESSAGE_FAILURE = 'FETCH_DECODED_MESSAGE_FAILURE';

export const fetchDecodedMessageRequest = () => ({type: FETCH_DECODED_MESSAGE_REQUEST});
export const fetchDecodedMessageSuccess = message => ({type: FETCH_DECODED_MESSAGE_SUCCESS, payload: message});
export const fetchDecodedMessageFailure = () => ({type: FETCH_DECODED_MESSAGE_FAILURE});

export const fetchDecodeMessage = () => {
	return async dispatch => {
		try {
			dispatch(fetchDecodedMessageRequest());
			const response = await axios.get('http://127.0.0.1:8000/decode');
			dispatch(fetchDecodedMessageSuccess(response.data));
		} catch (e) {
			dispatch(fetchDecodedMessageFailure());
		}
	};
};