import axios from "axios";

export const SEND_MESSAGE_REQUEST = 'SEND_MESSAGE_REQUEST';
export const SEND_MESSAGE_SUCCESS = 'SEND_MESSAGE_SUCCESS';
export const SEND_MESSAGE_FAILURE = 'SEND_MESSAGE_FAILURE';

export const sendMessageRequest = () => ({type: SEND_MESSAGE_REQUEST});
export const sendMessageSuccess = () => ({type: SEND_MESSAGE_SUCCESS});
export const sendMessageFailure = () => ({type: SEND_MESSAGE_FAILURE});

export const sendMessage = (path, messageData) => {
	return async dispatch => {
		try {
			dispatch(sendMessageRequest());
			await axios.post(`http://127.0.0.1:8000/${path}`, messageData);
			dispatch(sendMessageSuccess());
		} catch (e) {
			dispatch(sendMessageFailure());
		}
	};
};