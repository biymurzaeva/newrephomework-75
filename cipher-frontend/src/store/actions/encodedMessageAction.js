import axios from "axios";

export const FETCH_ENCODED_MESSAGE_REQUEST = 'FETCH_ENCODED_MESSAGE_REQUEST';
export const FETCH_ENCODED_MESSAGE_SUCCESS = 'FETCH_ENCODED_MESSAGE_SUCCESS';
export const FETCH_ENCODED_MESSAGE_FAILURE = 'FETCH_ENCODED_MESSAGE_FAILURE';

export const fetchEncodedMessageRequest = () => ({type: FETCH_ENCODED_MESSAGE_REQUEST});
export const fetchEncodedMessageSuccess = message => ({type: FETCH_ENCODED_MESSAGE_SUCCESS, payload: message});
export const fetchEncodedMessageFailure = () => ({type: FETCH_ENCODED_MESSAGE_FAILURE});

export const fetchEncodeMessage = () => {
	return async dispatch => {
		try {
			dispatch(fetchEncodedMessageRequest());
			const response = await axios.get('http://127.0.0.1:8000/encode');
			dispatch(fetchEncodedMessageSuccess(response.data));
		} catch (e) {
			dispatch(fetchEncodedMessageFailure());
		}
	};
};