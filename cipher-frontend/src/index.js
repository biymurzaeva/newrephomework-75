import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import thunk from "redux-thunk";
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import decodedMessageReducer from "./store/reducers/decodedMessageReducer";
import encodedMessageReducer from "./store/reducers/encodedMessageReducer";

const rootReducer = combineReducers({
	'decodedMsg': decodedMessageReducer,
	'encodedMsg': encodedMessageReducer
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducer, composeEnhancers(
	applyMiddleware(thunk)
));

const app = (
	<Provider store={store}>
		<BrowserRouter>
			<App/>
		</BrowserRouter>
	</Provider>
);

ReactDOM.render(app, document.getElementById('root'));
