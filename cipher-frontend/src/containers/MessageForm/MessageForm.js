import React, {useState} from 'react';
import {Button, Grid, TextField} from "@material-ui/core";
import {useDispatch, useSelector} from "react-redux";
import {fetchDecodeMessage} from "../../store/actions/decodedMessageAction";
import {sendMessage} from "../../store/actions/sendMessageAction";
import {fetchEncodeMessage} from "../../store/actions/encodedMessageAction";

const MessageForm = () => {
	const dispatch = useDispatch();
	const decodedMessage = useSelector(state => state.decodedMsg.message);
	const encodedMessage = useSelector(state => state.encodedMsg.message);

	const [state, setState] = useState({
		password: "",
		decode: "",
		encode: ""
	});

	const submitFormHandler = async e => {
		e.preventDefault();

		if (state.encode.length !== 0) {
			await dispatch(sendMessage('encode', {message: state.encode, password: state.password}));
			await dispatch(fetchDecodeMessage());
			setState(state => ({
				...state,
				decode: Object.keys(decodedMessage)[0]
			}));
		} else if (state.decode.length !== 0) {
			await dispatch(sendMessage('decode', {message: state.decode, password: state.password}));
			await dispatch(fetchEncodeMessage());
			setState(state => ({
				...state,
				encode: Object.keys(encodedMessage)[0]
			}));
		}
	};

	const inputChangeHandler = e => {
		const name = e.target.name;
		const value = e.target.value;
		setState(prevState => {
			return {...prevState, [name]: value};
		});
	};

	return (
		<form
			autoComplete="off"
			onSubmit={submitFormHandler}
		>
			<Grid container direction="column" spacing={2}>
				<Grid item xs>
					<TextField
						fullWidth
						variant="outlined"
						value={state.decode}
						label="Decoded message"
						onChange={inputChangeHandler}
						name="decode"
					/>
				</Grid>
				<Grid item xs>
					<TextField
						fullWidth
						variant="outlined"
						label="Password"
						value={state.password}
						onChange={inputChangeHandler}
						name="password"
					/>
					<Grid item xs>
						<Button type="submit" color="primary" variant="contained" className="Up">Up</Button>
						<Button type="submit" color="primary" variant="contained" className="Down">Down</Button>
					</Grid>
				</Grid>
				<Grid item xs>
					<TextField
						fullWidth
						multiline
						rows={3}
						variant="outlined"
						label="Encoded message"
						value={state.encode}
						onChange={inputChangeHandler}
						name="encode"
					/>
				</Grid>
			</Grid>
		</form>
	);
};

export default MessageForm;