import React from 'react';
import MessageForm from "./containers/MessageForm/MessageForm";
import {Route, Switch} from "react-router-dom";

const App = () => (
  <>
   <Switch>
     <Route path="/" exact component={MessageForm}/>
     <Route path="/encode" component={MessageForm}/>
     <Route path="/decode" component={MessageForm}/>
   </Switch>
  </>
);

export default App;
