const fs = require('fs');
const filename = './db.json';
let data = {};

module.exports = {
	init() {
		try {
			const fileContents = fs.readFileSync(filename);
			data = JSON.parse(fileContents);
		} catch (e) {
			data = [];
		}
	},
	getMessage() {
		return data;
	},
	addItem(item) {
		data = item ;
		this.save();
		return item;
	},
	save() {
		fs.writeFileSync(filename, JSON.stringify(data));
	}
};