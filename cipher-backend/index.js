const express = require('express');
const encode = require('./app/encode');
const decode = require('./app/decode');
const fileDb = require('./fileDb');
const cors = require('cors');

const app = express();
app.use(express.json());
const port = 8000;

app.use('/encode', encode);
app.use('/decode', decode);
app.use(cors());

fileDb.init();
app.listen(port, () => {
	console.log(`Server started on ${port} port!`);
});