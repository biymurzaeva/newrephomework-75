const express = require('express');
const fileDb = require("../fileDb");
const router = express.Router();

const Vigenere = require('caesar-salad').Vigenere;

router.get('/', (req, res) => {
	const msg = fileDb.getMessage();
	res.send(msg);
});

router.post('/', (req, res) => {
	if (!req.body.password || !req.body.message) {
		return res.status(404).send({error: "Data not valid"});
	}

	const decode = fileDb.addItem({"decoded": Vigenere.Decipher(req.body.password).crypt(req.body.message)});
	res.send(decode);
});

module.exports = router;