const express = require('express');
const router = express.Router();
const fileDb = require('../fileDb');

const Vigenere = require('caesar-salad').Vigenere;

router.get('/', (req, res) => {
	const msg = fileDb.getMessage();
	res.send(msg);
});

router.post('/', (req, res) => {
	if (!req.body.password || !req.body.message) {
		return res.status(404).send({error: "Data not valid"});
	}

	const encodeMsg = fileDb.addItem({"encoded": Vigenere.Cipher(req.body.password).crypt(req.body.message)});
	res.send(encodeMsg);
});

module.exports = router;